# Normaliation and Segmentation of CT imagery for DBS

The following code accompanies the paper "Where Position Matters: Improving Lead Localization in Stereotactic Neurosurgery".
## Install 
The code depends on the ANTS registration toolbox (**http://stnava.github.io/ANTs**) and the patchwork toolbox (**https://bitbucket.org/reisert/patchwork/wiki/Home**)
for deep learning based segmentation. 
 

## image variables
Below we explain the meaning of the image file variables used in the code. Necessary
template files are included in the repository.

```python
# images (assumed to be rigidly coregisterd)
CTIMG='CT.nii.gz'                 # the input CT image
T1IMG='T1.nii.gz'                 # the T1 weighted MRI
T2IMG='T2.nii.gz'                 # the T2 weighted MRI
# prediction of the CNN based on CT image
PREDC1='pred_c1.nii'              # gray matter predictions (probabilities)
PREDC2='pred_c2.nii'              # white matter predictions (probabilities)
PREDC3='pred_c3.nii'              # CSF predictions (probabilities)
PREDNUC='pred_nuc.nii'            # nuclei predictions (probabilities)
# SPM segmentation based on T1
SPMC1='c1T1.nii'                  # no code included to generate these
SPMC2='c2T1.nii'                  # you need SPM to generate them
SPMC3='c3T1.nii'                  # based your input T1

# templates in MNI space 
T1TPM='T1tpm.nii'
T2TPM='T2tpm.nii'
MIDBRAIN='midbrain.nii'          # a midbrain mask
# averages created from CT bsaed predictions in MNI space
C1TPM='PC1tpm.nii'               
C2TPM='PC2tpm.nii'
C3TPM='PC3tpm.nii'
NUCTPM='NUCtpm.nii'

```
## Prediction by CNN
The used CNN is implemented in python and is based on hierarchical patch-based U-nets.

### Nuclei

The following code predicts the deep gray matter nuclei (nucleus ruber, subthalamic nucleus, striatum). It outputs a
nifti volumetric image containing probabilities for nuclei presence.

```python
os.environ["CUDA_VISIBLE_DEVICES"] = "" # to run without GPU support
import patchwork.model as patchwork
model = patchwork.PatchWorkModel.load('nuclei/model_patchwork.json',immediate_init=False)
res,r = model.apply_on_nifti(CTIMG ,PREDNUC,generate_type='random',repetitions=64,num_chunks=40,scale_to_original=False,
lazyEval=0.5,branch_factor=2)

```

### GM/WM/CSF

The following code predicts jointly gray/white matter and CSF masks. As above, it outputs a
nifti volumetric image containing probabilities for class presence.

```python

model = patchwork.PatchWorkModel.load('comps/model_patchwork.json',immediate_init=False)
res,r = model.apply_on_nifti(CTIMG ,[PREDC1,PREDC2,PREDC3],generate_type='random',repetitions=64,num_chunks=40,scale_to_original=False,
window='cos2',level='mix')
```

## Registration/Normalization of the CT 

### CT MRI coreg

The following ANTS command co-registers CT and MRI based on cross correlations of compartmental probabilities 
derived from CT (via the above CNN) and segmentations based on the MRI (e.g. using SPM12).

```bash
antsRegistration -d 3 --float 1 -v 1 \
		--transform SyN[0.1,3,1] -m CC["$SPMC1","$PREDC1"  ,1,4] \
                                 -m CC["$SPMC2","$PREDC2"  ,1,4] \
                                 -m CC["$SPMC3","$PREDC3"  ,1,4] \
                                 -m MI["$T1","$CTIMG"  ,1,32,Regular,0.25] \
        --convergence [100x70x20,1e-4,10] \
        --shrink-factors 4x2x1 \
        --smoothing-sigmas 3x1x0vox \
        -o "ct2mri" 

```

### CT to MNI

The following ANTS command normalizes the CT into MNI space by using the predicted compartmental/nuclei probabilities 
onto averaged templates in MNI2009b_asym space.

```bash
antsRegistration -d 3 --float 1 -v 1 --initial-moving-transform ["$C1TPM","$PREDC1"  ,1] \
		--transform Rigid[0.1]  -m MI["$C1TPM","$PREDC1"  , 1, 32] -c 0 -f 4 -s 2  \
        --transform Affine[0.1] -m MI["$C1TPM","$PREDC1"   ,1,32,Regular,0.25]  \
		 --convergence [1000x500x250x100,1e-6,10]  \
         --shrink-factors 8x4x2x1  \
         --smoothing-sigmas 3x2x1x0vox \
		--transform SyN[0.1,3,0] -m CC["$C1TPM","$PREDC1"  ,1,4] \
                                 -m CC["$C2TPM","$PREDC2"  ,1,4] \
                                 -m CC["$C3TPM","$PREDC3"  ,1,4] \
                                 -m CC["$NUCTPM","$PREDNUC"  ,1,4] \
          --convergence [100x70x20,1e-4,10] \
          --shrink-factors 8x4x2  \
          --smoothing-sigmas 3x2x1vox \
          -o "ct2tpm" 
```

### CT bias correction

The MNI spaces induced by CT or MRI are slightly different. We include the warp fields connecting these two 
spaces. Here you can see the code, how to tansform the CT induced space into the MRI induced one.

```bash
antsApplyTransforms -v -i ct2tpm1InverseWarp.nii.gz -r ct2tpm1InverseWarp.nii.gz -d 3 \
          -t ct2tpm1InverseWarp.nii.gz \
          -t biascorrInverseWarp.nii \
          -o [ct2tpm_biascorr_1InverseWarp.nii,1]

antsApplyTransforms -v  -i ct2tpm1Warp.nii.gz -r ct2tpm1Warp.nii.gz -d 3 \
             -t biascorrWarp.nii \
             -t ct2tpm1Warp.nii.gz \
             -o [ct2tpm_biascorr_1Warp.nii,1]

```


## Normalization via classical MRI contrasts

### T1w/T2w to MNI 

The following code runs a standard normalization to MNI2009b_asym via the T1w and Tw2 MRI contrast. 
Parameters are chosen such that the normlization is relatively fast and identical to the one used in LeadDBS.
In the final stage the normlization takes particular care of the midbrain area by using a mask.

```bash

antsRegistration -d 3 --interpolation Linear --use-histogram-matching 0 --float 1 --winsorize-image-intensities [0.005,0.995] 
        --initial-moving-transform [$T1TPM,$T1IMG,0]  \
           --transform Rigid[0.25] \
              --convergence [1000x500x250x0,1e-7,10] \
              --shrink-factors 12x8x4x1 --smoothing-sigmas 5x4x3x1vox \
              --metric MI["$T2TPM",$T2IMG,1.25,32,Random,0.25] \
              --metric MI["$T1TPM",$T1IMG,1.25,32,Random,0.25] \
           --transform Affine[0.15] \
              --convergence [1000x500x250x0,1e-7,10] \
              --shrink-factors 12x8x4x1 --smoothing-sigmas 5x4x3x1vox \
              --metric MI["$T2TPM",$T2IMG,1.25,32,Random,0.25] \
              --metric MI["$T1TPM",$T1IMG,1.25,32,Random,0.25] \
           --transform SyN[0.3,4,3] \
              --convergence [1000x500x250x0,1e-6,7] \
              --shrink-factors 8x4x4x1 --smoothing-sigmas 4x3x1x1vox\
              --metric MI["$T2TPM",$T2IMG,1.25,32,Random,0.25] \
              --metric MI["$T1TPM",$T1IMG,1.25,32,Random,0.25] \
           --transform SyN[0.3,4,3] \
              --convergence [200x50x10x0,1e-6,7] \
              --shrink-factors 4x4x2x1 --smoothing-sigmas 2x2x1x1vox \
              --use-estimate-learning-rate-once  \
              --masks ["$MIDBRAIN",NULL] \
              --metric MI["$T2TPM",$T2IMG,1.25,32,Random,0.25] \
              --metric MI["$T1TPM",$T1IMG,1.25,32,Random,0.25] \
           -o "mri2tpm_iii"


```











